<?php 
  /*
  Template Name: Start
  */
?>

<?php get_header(); ?>

  <div class="intro">
    <?php get_template_part('parts/content', 'page'); ?>
  </div>

  <?php get_template_part('modules/newsletter'); ?>

  <?php get_template_part('parts/content', 'posts'); ?>
  <p class="read-more-section"><a href="/blog" title="Zum Blog" class="button">Alle Artikel</a></p>
<?php get_footer(); ?>