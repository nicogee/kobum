<?php get_header(); ?>
  
  <section class="posts">

    <?php if(have_posts()): while(have_posts()): the_post(); ?>
        
      <article class="post grid">

        <header class="post-header">
          <hgroup>
            <div class="post-meta">
              <p class="info"><?php the_date() ?> &bull; <?php kobum_post_categories() ?></p>
            </div>
            <h1 class="post-title"><?php the_title() ?></h1>
          </hgroup>
        </header>

          <?php the_content(); ?>
        </article>
        
    <?php endwhile; endif; ?>
    <div class="pagination">
      <?php next_post_link('%link', '&laquo;'); ?>
      <?php previous_post_link('%link', '&raquo;'); ?>
    </div>
  </section>
  <?php get_template_part('modules/newsletter'); ?>
<?php get_footer(); ?>