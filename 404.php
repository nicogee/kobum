<?php get_header(); ?>

    <section class="grid">
      
        <h1 class="page-title">Whoops</h1>
        <article class="post aligncenter">
          <p class="center">Diese Seite wurde nicht gefunden.</p>
        </article>

      </section>
<?php get_footer(); ?>