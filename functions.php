<?php

// show_admin_bar( false );


add_action('init', 'graff_init');
add_action('wp_enqueue_scripts', 'graff_scripts');

function graff_init() {
  add_theme_support('post-thumbnails');
  add_image_size( 'category-thumb', 470, 470, true );
  register_my_menu();
}

function graff_scripts() {
  wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/style.min.css');
  wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.min.js', array('jquery'), 1, true);
  
}

function register_my_menu() {
  register_nav_menu('primary', __( 'Main Menu' ));
}

function get_latest_articles() {
  global $post;
  
  $articles = get_posts([
    'exclude' => $post->ID,
    'posts_per_page' => 3
  ]);

  return $articles;
}

function new_excerpt_more($content) {
  return $content;
}

add_filter('excerpt_more', 'new_excerpt_more');


function kobum_post_categories() {

  if ( 'post' === get_post_type() ) {
    
      $categories_list = get_the_category_list(', ');
      if ( $categories_list ) {
        printf( '<span class="cat-links">' . '%1$s' . '</span>', $categories_list );
      }

     
      $tags_list = get_the_tag_list( '', ', ' );
      if ( $tags_list ) {
        printf( ' &bull; <span class="tags-links">' . '%1$s' . '</span>', $tags_list );
      }
  }

}

function the_category_name() {
  $category = get_the_category(); 
  echo $category[0]->cat_name;
}

function the_category_list() {
  $cats = get_categories();
  $cats_list = '';

  foreach($cats as $cat) {
    $cats_list .= '<a href="' . get_category_link( $cat->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $cat->name ) . '" ' . '>' . $cat->name.'</a>, ';
  }

  echo rtrim($cats_list, ', ');
}

function get_custom_posttype($posttype, $limit = null) {
   
  if($limit == null) {
    $limit = get_option( 'posts_per_page' );
  }

  $option = array(
    'post_type' => $posttype,
    'posts_per_page' => $limit,
    'post_status' => 'publish'
  );

  $query = new WP_Query($option);
  return $query;
}

function get_articles($post_ids) {
  $query = new WP_Query( array('post_type' => 'post', 'post_status' => 'publish', 'post__in' => $post_ids) );
  return $query;
}

function check_for_px($v) {

  if($v != 'auto') {
    $v = $v.'px';
  }

  echo $v;
}

add_filter('embed_oembed_html', 'my_embed_oembed_html', 99, 4);

function my_embed_oembed_html($html, $url, $attr, $post_id) {
  return '<div class="video-container">' . $html . '</div>';
}


// 1. customize ACF path
add_filter('acf/settings/path', 'my_acf_settings_path');
 
function my_acf_settings_path( $path ) {
 
    // update path
    $path = get_stylesheet_directory() . '/acf/';
    
    // return
    return $path;
    
}


// 2. customize ACF dir
add_filter('acf/settings/dir', 'my_acf_settings_dir');
 
function my_acf_settings_dir( $dir ) {
 
    // update path
    $dir = get_stylesheet_directory_uri() . '/acf/';
    
    // return
    return $dir;
    
}
 

// 3. Hide ACF field group menu item
add_filter('acf/settings/show_admin', '__return_false');


// 4. Include ACF
include_once( get_stylesheet_directory() . '/acf/acf.php' );


// ACF PARTNER PAGE

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title' => 'KOBUM Partner',
    'menu_title' => 'KOBUM Partner',
    'menu_slug'   => 'kobum-partner-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
}

?>