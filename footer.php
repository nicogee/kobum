<section class="footer footbridge top">

    <footer class="grid">
      <?php get_template_part('modules/partner'); ?>
    </footer>

  </section> <!-- .footer -->
  
  
  <section class="footer-bar">
    <div class="grid">
    <a href="/impressum" title="Impressum">Impressum</a> &bull; 
    <a href="/impressum#datenschutz" title="Datenschutz">Datenschutz</a> &bull; 
    <a href="mailto:info@kobum.de" title="Kontakt">Kontakt</a>
    </div>
  </section>

</div> <!--// #container -->
<?php wp_footer(); ?>
</body>
</html>