<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title><?php wp_title(); ?></title>
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() ?>/assets/images/favicon-32x32.png" sizes="32x32">
  <?php wp_head(); ?>
</head>
<body <?php body_class() ?>>
<img width="0" height="0" src="http://uip.semasio.net/echteliebe/1/info?sType=track&nTrackingPointId=35649&_sdv" class="omp" />
<img src="https://track.adform.net/Serving/TrackPoint/?pm=1024727&ADFPageName=Kobum-HP-Sep17&ADFdivider=|" width="1" height="1" alt="" class="omp" />
<div id="container">
  <header id="header">
    <?php get_template_part('modules/slider'); ?>
    <div class="header-inner grid flex">
      
      <a href="<?php bloginfo('home'); ?>" title="KOBUM - Start" id="logo"><?php bloginfo('name'); ?></a>
      <button type="button" class="hide-on-desktop hamburger" id="mobile-nav"><span class="h-icon"></span></button>

      <div class="hide-on-mobile mobile-nav">
        <?php 
          wp_nav_menu([
            'theme_location' => 'primary',
            'container' => 'nav',
            'container_class' => 'main-nav',
            'menu_id' => 'main-menu'
          ]);
        ?>

        <div class="social-icons">
          <a href="https:/facebook.com/kobum2017" title="Kobum auf Facebeook" class="social icon-facebook" target="_blank"></a>
          <a href="https:/instagram.com/kobum_cgn" title="Kobum bei Instagram" class="social icon-instagram" target="_blank"></a>
          <a href="https://www.youtube.com/channel/UCaZ3ei3cvG_qdmZhVRsldZA" title="Kobum bei Youtube" class="social icon-youtube" target="_blank"></a>
        </div> <!-- social-icons -->

      </div>
    </div><!-- header-inner-->

  </header><!-- /header -->