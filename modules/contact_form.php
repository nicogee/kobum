<section class="contact" id="kontakt">
  <h1 class="page-title"><?php the_sub_field('title'); ?></h1>
  <?php echo do_shortcode('[graffcontact]'); ?>
</section> <!-- .contact -->