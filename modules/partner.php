<?php if( have_rows('kobum_partner', 'option') ): ?>

  <div class="partner flex">
          
    <h2 class="page-title">Partner</h2>

    <?php while(have_rows('kobum_partner', 'option')): the_row(); ?>
      <div class="item">
        
        <?php if(!empty(get_sub_field('kobum_partner_url'))): ?>
          <a href="<?php the_sub_field('kobum_partner_url') ?>">
        <?php endif; ?>

        <img src="<?php echo get_sub_field('kobum_partner_logo', 'option')['url'] ?>" alt="<?php echo get_sub_field('kobum_partner_logo', 'option')['alt']?>" style="width:<?php check_for_px(get_sub_field('kobum_partner_width', 'option')) ?>;height:<?php check_for_px(get_sub_field('kobum_partner_height', 'option'))?>">
        
        <?php if(!empty(get_sub_field('kobum_partner_url'))): ?></a><?php endif; ?>

      </div>
    <?php endwhile; ?>

  </div> <!-- partner -->

<?php endif; ?>