<div class="newsletter-container footbridge top bottom">
  <form action="https://kobum.us17.list-manage.com/subscribe/post?u=57e16d202c773644d9e098e11&id=7bc9e2857b" method="get" accept-charset="utf-8" class="form grid" id="newsletter-form" target="_blank">
      <p><strong class="nl-title">Newsletter abonnieren, Nichts verpassen!</strong></p>
      <div class="form-inner">
        <input type="text" name="name" id="nl_name" placeholder="Name" required="required">
        <input type="email" name="email" id="nl_email" placeholder="Email" required="required">
        <button type="submit" class="button">abonnieren</button>
      </div>
      <div class="submit-message"></div>
  </form>
</div>