<?php if($custom_posts->have_posts()): ?>

  <section class="references" id="referenzen" style="background-color: <?php the_sub_field('background_color') ?>">
    <div class="grid">
    <h1 class="page-title"><?php the_sub_field('title'); ?></h1>

  <?php while($custom_posts->have_posts()): $custom_posts->the_post(); ?>

    <div class="reference">
      <div><?php the_post_thumbnail('full'); ?></div>
      <div class="details">
        <h3><?php the_title() ?></h3>
        <?php the_content(); ?>
      </div>
    </div> <!-- .reference -->

  <?php wp_reset_postdata(); endwhile; ?>

    </div> <!-- .grid -->
  </section> <!-- .references -->
<?php endif;?>