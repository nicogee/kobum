<section id="<?php get_sub_field('css_id') ? the_sub_field('css_id') : ''; ?>" class="posts" style="background-color: <?php the_sub_field('background_color') ?>">
          
  <div class="grid flex">

    <h1 class="page-title"><?php the_sub_field('title'); ?></h1>
    
    <?php if(get_sub_field('image')): ?>
      <div class="image">
        <img src="<?=get_sub_field('image')['url']; ?>" class="item" alt="<?=get_sub_field('image')['title']; ?>">
      </div> <!-- .image -->
    <?php endif; ?>

    <div class="details item">
      <?php the_sub_field('post'); ?>
    </div><!-- .details -->
    
  </div>

</section>