<?php 

$custom_posts = get_custom_posttype( get_sub_field('posttype'), get_sub_field('number_of_posts') );

if(get_sub_field('posttype') == 'references'):
  include( locate_template('/modules/references.php') );
else: ?>

  <section class="posttypes" style="background-color: <?php the_sub_field('background_color') ?>">
    <div class="grid flex">

    <h1 class="page-title"><?php the_sub_field('title'); ?></h1>

    <?php while($custom_posts->have_posts()): $custom_posts->the_post(); ?>

      <div class="item post-format-<?php echo get_post_format($custom_post->ID) ?>">
        <h2><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></h2>
        <div class="post-content">
          <?php the_content(''); ?>
        </div>
        <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">weiter lesen</a>
      </div>

    <?php wp_reset_postdata(); endwhile; ?>

    </div> <!-- grid flex -->
  </section> <!-- .solutions -->

<?php endif; ?>