<?php  

if(function_exists('get_field') && $images = get_field('g_header_slider')): ?>
<div class="header-inner-top">

  <div class="header-logo-date">
    <a href="<?php echo get_field('g_header_breaker_link') ? get_field('g_header_breaker_link') : '#'?>">
      <?php if(get_field('g_header_breaker')): $brimage = get_field('g_header_breaker'); ?>
        <img src="<?php echo $brimage['url'] ?>" alt="<?php echo $brimage['alt'] ?>" class="logo-date">
      <?php endif; ?>
    </a>
  </div>
  <div class="slider-for">
    <?php foreach( $images as $image ): ?>
        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
    <?php endforeach; ?>
  </div>

</div><!-- header-inner-top -->
<?php endif; ?>