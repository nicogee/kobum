<?php

  if(function_exists('get_field')) {
   
    if(have_rows('modules')) {
        
       while ( have_rows('modules') ) : the_row();
       
        if(get_row_layout() == 'posttypes'):
          
          get_template_part('/modules/posttypes');

        endif; //posttypes

        if(get_row_layout() == 'contact_form'):
          
          get_template_part('/modules/contact_form');

        endif; //posttypes

        if(get_row_layout() == 'content'):
          
          get_template_part('/modules/content');

        endif; //posttypes

       endwhile; //have_rows

    }

  } //end function_exists


?>