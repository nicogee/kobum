// @koala-prepend "slick.min.js"
// @koala-prepend "jquery.mailchimp.js"
// @koala-prepend "simplelightbox.min.js"

var contact = document.getElementById('contact_form');
var form_message = document.getElementById('form_message');
var button = contact ? contact.querySelector('button') : false;


jQuery(function($) {

  $('#mobile-nav').on('click', function() {
    $(this).toggleClass('open').next('.mobile-nav').toggleClass('visible');
  });

  $('.slider-for').slick({
    autoplay: !1,
    arrows: !1,
    dots: !0
  });

  $('#newsletter-form').ajaxChimp({
     language: 'de'
  });

  var lightbox = $('.gallery a').simpleLightbox({
    captionsData : 'alt',
    captionPosition: 'outside'
  });

});


