<?php get_header(); ?>
  
  <?php if (has_post_thumbnail()): ?>
      <?php the_post_thumbnail(); ?>
  <?php endif ?>

  <section class="posts grid">

    <?php if(have_posts()): while(have_posts()): the_post(); ?>
        <header class="post-header">
            <h1 class="page-title"><?php the_title() ?></h1>
        </header>

        <article class="post wrap">
          <?php the_content(); ?>
        </article>

    <?php endwhile; endif; ?>
  </section>
  
<?php get_footer(); ?>