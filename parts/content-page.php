<section class="posttypes">

  <div class="grid">
     <h1 class="page-title"><?php the_title(); ?></h1>
    <?php if(have_posts()): while(have_posts()): the_post(); ?>
        
        <article>
         
          <div class="post-content">
            <?php the_content(''); ?>
          </div>
        </article>

    <?php endwhile; endif; ?>
  </div> <!-- grid flex -->

</section><!-- posttypes -->