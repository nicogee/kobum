<?php $custom_posts = get_custom_posttype('post', 4); ?>
<section class="posttypes posts-start">
  <div class="grid flex">
    <h1 class="page-title">Blog</h1>
    <?php while($custom_posts->have_posts()): $custom_posts->the_post(); ?>
      
      <?php if(has_post_thumbnail() && in_category('bilder')): ?>

         <article class="item posttype-images">
          <h2 class="post-title">
            <a href="<?php the_permalink() ?>" title="<?php the_title() ?>"">
              <?php the_title(); ?>
            </a>
          </h2>
          <a href="<?php the_permalink()?>" title="Artikel lesen: <?php the_title() ?>" class="post-content" style="background-image:url(<?php the_post_thumbnail_url('category-thumb') ?>)">
          </a>
        </article>

      <?php else: ?>

        <article class="item">
          <h2 class="post-title">
            <a href="<?php the_permalink() ?>" title="<?php the_title() ?>"">
              <?php the_title(); ?>
            </a>
          </h2>
          <?php if(has_post_thumbnail()): the_post_thumbnail('category-thumb'); endif; ?>
          <div class="post-content">
            <?php the_content(''); ?>
          </div>
          <a href="<?php the_permalink()?>" title="Artikel lesen: <?php the_title() ?>" class="read-more">weiter &gt;</a>
        </article>
        
      <?php endif; ?>

    <?php endwhile; wp_reset_postdata(); ?>
  </div> <!-- grid flex -->

</section><!-- posttypes -->