  <div class="wrap author-box">
    <div class="grid clear">
      
      <?php  echo get_avatar(get_the_author_meta( 'ID')) ?>
      <div class="author-infos">
        <a href="/about-me/"><?php the_author_meta( 'display_name') ?></a>
        <?php the_author_meta( 'user_description') ?>
      </div>

    </div>
  </div>