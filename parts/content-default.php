<section class="posttypes">

  <div class="grid flex">
    <?php if(have_posts()): while(have_posts()): the_post(); ?>
        
        <article>
          <h1 class="page-title">
            <a href="<?php the_permalink() ?>" title="title="<?php the_title() ?>"">
              <?php the_title(); ?>
            </a>
          </h1>
          <div class="post-content">
            <?php the_content(''); ?>
          </div>
        </article>

    <?php endwhile; endif; ?>
  </div> <!-- grid flex -->

</section><!-- posttypes -->