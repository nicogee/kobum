<div class="latest-articles grid">

  <?php $next_post = get_next_post();

  if (!empty( $next_post )): ?>
      <a href="<?php echo get_permalink( $next_post->ID ); ?>" class="prev-post">
        <strong><?php echo $next_post->post_title; ?></strong>
        <span>vorheriger Artikel</span>
      </a>
  <?php endif; ?>

  <?php $prev_post = get_previous_post();
  if (!empty( $prev_post )): ?>
      <a href="<?php echo get_permalink( $prev_post->ID ); ?>" class="next-post">
        <strong><?php echo $prev_post->post_title; ?></strong>
        <span>nächster Artikel</span>
      </a>
  <?php endif; ?>
</div>